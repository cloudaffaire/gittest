# Markdown Syntax

## Paragraph and headings:

This is a short paragraph (p)

This is a long paragraph where we have not given a line break as a result, it will be rendered into a single block in your readme.md file. If you want to write a long paragraph block, do not break that into multiple paragraphs.

This is a long paragraph where we have given a line break. 

As a result, it will be rendered into multiple blocks in your readme.md file.

<!--- And this block will not get rendered, not visible in readme.md and used as comment --->

# This is Heading1 (H1)
## This is Heading2 (H2)
### This is Heading3 (H3)
#### This is Heading4 (H4)
##### This is Heading5 (H5)
###### This is Heading6 (H6)

## Text formatting:

This is a plain text

This is a **bold** text

This is an _italic_ text

This is a ~~strike through~~ text

This is a `code` block

**This is _italic_ within bold text**

**This is ~~strike through~~ within bold text**

_This is ~~strike through~~ within italic text_

***This is bold and italic text***

**~~This is bold and strike through text~~**

_~~This is italic and strike through text~~_

> This is a quote block
>> This is a quote block within quote block

## Footnote:

Markdown is a lightweight markup language[^1] for creating formatted text.

John Gruber[^2] and Aaron Swartz[^3] created Markdown in 2004 as a markup language

Markdown language is used to write README[^readme] files.

[^1]: A lightweight markup language (LML), also termed a simple or humane markup language, is a markup language with simple, unobtrusive syntax. 
  It is designed to be easy to write using any generic text editor and easy to read in its raw form. 
  Lightweight markup languages are used in applications where it may be necessary to read the raw document as well as the final rendered output.
[^2]: John Gruber (born 1973) is a technology blogger, UI designer, and one of the inventors of the Markdown markup language. 
[^3]: Aaron Hillel Swartz (November 8, 1986 – January 11, 2013) was an American computer programmer, entrepreneur, writer, political organizer, and Internet hacktivist. He was involved in the development of the web feed format RSS, the Markdown publishing format, the organization Creative Commons, and the website framework web.py, and joined the social news site Reddit six months after its founding.
[^readme]:
    A README file contains information about the other files in a directory or archive of computer software.
    A form of documentation, it is usually a simple plain text file called README, Read Me, READ.ME, README.TXT, README.md (to indicate the use of Markdown), or README.1ST

## Horizontal rules

---
line one
***
line two
___
line three
- - - -
line four
* * * *
line five
_ _ _ _

## Code snippet 

Use `echo Hello World!` command to print in shell

Use below code to print in java
```
class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!"); 
    }
}
```

Same java code with syntax highlight
```java
class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!"); 
    }
}
```

## List

### Ordered List
1. First item
2. Second item
    1. First sub item
    2. Second sub item
        1. First sub sub item
3. Third item
4. Fourth item

### Unordered List
- First item
- Second item
    - First sub item
    - Second sub item
        - First sub sub item
- Third item
- Fourth item

### Task List
- [x] First item
- [ ] Second item
    - [ ] First sub item
    - [x] Second sub item
        - [x] First sub sub item
- [ ] Third item
- [ ] Fourth item
